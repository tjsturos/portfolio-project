const sanding = "If you start to see wear spots in the floor that are darker in color or a greyish color more then likely the finish has been worn through and its now down to raw wood, now is the time to sand your floor down" +
" and have it sealed again. If a person waits to long it could lead up to board replacement and just a " +
"longer process overall and more expensive. " +

`At oak ridge we use dustless sanding equipment to keep ` +
"dust down to the barest minimum; even though it’s dustless you can never actually get away from all " +
"dust. We also poly off cabinets and doorways when possible to make sure the little dust that does come " +
"out is contained. As we move forward with the project the poly comes down right before final coat, " +
"everything gets a final wipe down and a very thorough vacuum job." +
"We can coat the floor with natural " +
"finish whether it is water based finish or oil based finish to see the wood in its natural beauty, or you " +
"could go with a stain and completely change the color of your floor. If we stain your floor we will do " +
"samples onsite and go over various color options with you until you have a color that you love."

export default sanding; 