import sanding from './sanding-refinishing.js'

const descriptions = {
    "Sanding/Refinishing" : sanding,
    "Patching/Interlacing": require('./patching.txt'),
    "Custom Flooring": require('./custom.txt'),
    "Repairs": require('./repairs.txt'),
    "Click Together/Floating Floor" : require('./clickTogether.txt'),
    "Glue Down" : require('./glueDown.txt'),
    "Stain Matching" : require('./stainMatching.txt'),
    "Buff and Coat" : require('./buff-and-coat.txt'),
    "Hardwood Floor Install" : require('./hardwoodFloorInstall.txt'),
    "Prefinished Flooring" : require('./prefinishedFlooring.txt')
}

module.exports = { descriptions };