import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from '../components/Header';
import Services from '../components/Services';
import HomePage from '../components/HomePage';
import AboutPage from '../components/AboutPage';
import ContactPage from '../components/ContactPage';
import NotFoundPage from '../components/NotFoundPage';
import Footer from '../components/Footer';
import Gallery from '../components/Gallery';

const AppRouter = () => (
    
    <BrowserRouter>
        <div className="container-fluid background ">
            <Header />
            <div className="main-div">
                <Switch>
                    <Route path='/' component={HomePage} exact={true} />
                    <Route path="/about" component={AboutPage} exact={true}/>
                    <Route path="/services" component={Services} exact={true}/>
                    <Route path="/contact" component={ContactPage} exact={true} />
                    <Route path="/gallery"  component={Gallery} exact={true} />
                    <Route path="*" component={NotFoundPage} />
                </Switch>
            </div>
            <Footer />
        </div>
    </BrowserRouter>
);

export default AppRouter;