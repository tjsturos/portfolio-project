import React from 'react';
import ReactDOM from 'react-dom';

//components
import AppRouter from './routers/AppRouter';

//css
import 'normalize.css/normalize.css'
import './styles/styles.scss';

ReactDOM.render(<AppRouter />, document.getElementById('app'));