import React from 'react'
import axios from 'axios';
require("babel-polyfill");

const formatPictures = () => {

    
   
    let pictures = []
    for (let index = 0; index < 30; index++) {
        try {
            const picture = require(`../../public/pictures/${index}-image.JPG`) || require(`../../public/pictures/${index}-image.jpg`)

            if (picture) {
            pictures.push(
                <div className={index === 0 ? "carousel-item active" : "carousel-item"} key={index}>
                    <img className="d-block mh-100 img-fluid mx-auto rounded" src={picture} alt={`slide ${index}`} />
                </div>
            )
            }
        } catch (e) {
            console.log("picture doesn't exist")
        }
        
    }
    
    return pictures

}

const Gallery = () => {
    return (
        <div className="container">

            <div id="carouselExampleIndicators" className="carousel slide gallery container card" data-ride="carousel">
                <h2>Here are some examples of previous work:</h2>
                <h3>Click on the image to view full image.</h3>
                <br />
                <div className="carousel-inner gallery__pictures card-body container">
                    {formatPictures()}
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <i className="material-icons black-link" aria-hidden="true">arrow_back</i>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <i className="material-icons black-link">arrow_forward</i>
                    <span className="sr-only">Next</span>
                </a>
            </div>

        </div>
        
    )
}

export default Gallery;