import React from 'react'
import { render } from 'react-dom'
import { withFormik, Form, Field } from 'formik'
import Yup from 'yup'
import sendMessage from '../api/sendMessage';

const ContactComponent = ({
  values,
  errors,
  touched,
  isSubmitting,
  handleChange,
  status = {
    isSent: false
  }
}) => (
  status.isSent ? <div>Quote request sent!</div> : 
  <Form>
        <div className="input-group quote-input">
            { touched.firstName && errors.firstName && <p className="alert alert-danger" >{errors.firstName}</p> }
            <Field type="text" name="firstName" placeholder="First Name"/>
        </div>

        <div className="input-group quote-input">
            { touched.lastName && errors.lastName && <p className="alert alert-danger" >{errors.lastName}</p> }
            <Field type="text" name="lastName" placeholder="Last Name"/>
        </div>

        <div className="input-group quote-input">
            { touched.phone && errors.phone && <p className="alert alert-danger" >{errors.phone}</p> }
            <Field type="text" name="phone" placeholder="Phone"/>
        </div>

        <div className="input-group quote-input">
            { touched.location && errors.location && <p className="alert alert-danger" >{errors.location}</p> }
            <Field type="text" name="location" placeholder="City"/>
        </div>
   
    
    <button className="btn-lg btn btn-dark submit-button" disabled={isSubmitting}>Submit</button>
    
  </Form>
)

const ContactForm = withFormik({
  mapPropsToValues({ phone, firstName, lastName, location }) {
    return {
      phone: phone || '',
      firstName: firstName || '',
      lastName: lastName || '',
      location: location || ''
    }
  },
  validationSchema: Yup.object().shape({
      firstName: Yup.string().required("First Name is required!").min(2, "You need at least 2 characters for a first name!"),
      lastName: Yup.string().required("At least an initial is required!").min(1, "You need at least 2 characters for a last name!"),
      location: Yup.string().required("A location is required!"),
      phone:  Yup.string().required('Phone number is required.')
  }),
  handleSubmit(values, { setStatus, resetForm, setErrors, setSubmitting }) {
    console.log("Values: ", JSON.stringify(values, null, 2));
    const message = {
      to: "7633989461",
      from: "7723616980",
      body: `Automated Quote Request (DO NOT REPLY)\n\nName: ${values.firstName} ${values.lastName}\n
              Phone: ${values.phone}\n
              Location: ${values.location}`
    }
    try {
      if (sendMessage(message)){
        resetForm()
        setStatus({ isSent: true })
      } else setSubmitting(false);
      
    } catch(e) {
      console.log(e);
    }
   
  }
})(ContactComponent)

export default ContactForm;