import React from 'react'
import Gallery from './Gallery'

const AboutPage = (props) => (
    <div className="container">
        <div className="rounded card about">
            <h2><i>We know you want to know who does your flooring, so here is a bit about us:</i></h2>
            <p className="card-body">
            <b>Oak Ridge Hardwoods</b> is a family owned small business that operates in the Twin Cities and
            Greater Minnesota area. Trent grew up working in hardwood flooring.
            Trent's uncle ran a hardwood flooring company which he and several of his cousins worked at.<br/><br/>
            Trent worked doing hardwood throughout high school and in the following years, until he
            decided to build his own flooring company.<br/><br/>In 2016, Trent decided to establish Oak
            Ridge Hardwood. Accumulatively, Trent boasts {18 + (new Date().getFullYear() - 2017)} years of experience and counting. Both owners take
            pride in producing high quality floors, and building and maintaining lasting relationships.
            </p>
        </div>
    </div>
)

export default AboutPage;