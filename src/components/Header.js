import React from 'react';
import { NavLink } from 'react-router-dom';

import logo from '../../public/images/logo.png';
import QuoteComponent from './QuoteComponent';

const pages = ["Home", "Services", "About", "Gallery", "Contact"];

export default class Header extends React.Component {

    

    linkButtons = () => {
        return pages.map((link, index) => {
            return (
                <li className="nav-item" key={index} >
                    <NavLink to={`/${link === "Home" ? '' : link.toLowerCase()}`} className="nav-link" activeClassName='active'>{link}</NavLink>
                </li>
            )
        });
    };

    render() {
        return (
            <div className="container">
                    <img src="images/header.png" className="main-image"/>
               
                 <nav className="container navbar navbar-expand-lg navbar-dark bg-dark nav-bar">
                    
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    
                    <div className="collapse navbar-collapse row" id="navbarNav">
                    <div className="navbar-nav container">
                        <div className="col-auto mr-auto">
                            <ul className="navbar-nav ">
                                {this.linkButtons()}
                            </ul>
                        </div>
                        
        {/*<div className="nav-link my-2 my-lg-0 col-auto navbar-nav quote-button" onClick={e => this.setState((prevState) => ({ showQuote : !prevState.showQuote}))}>Request A Quote</div>*/}

                    </div>

                    </div>
                
                </nav>
            </div>
        )
    }

}