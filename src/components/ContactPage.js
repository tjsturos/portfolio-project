import React from 'react';

import ContactForm from './QuoteComponent';

const ContactPage = () => (
    <div className="container ">
        <div className="container rounded bg-white contact">
        
        <address className="address-box justify-content-center">
            <h3><i className="material-icons" alt="Home">home</i></h3>
            <b>Oak Ridge Hardwood</b><br/>
            1116 County Road 3 SW <br/>
            Cokato, MN 55321<br/>
            <br/>
            <h3><i className="material-icons" alt="Telephone">phone</i></h3>
            <a className="black-link" href="tel:+1-320-224-6115">Trent: (320) 224-6115</a><br/>
            <br/>
            <h3><i className="material-icons" alt="Email">email</i></h3>
            oakridgehardwoodmn@gmail.com

        </address>
        </div>
    </div>
)

export default ContactPage;