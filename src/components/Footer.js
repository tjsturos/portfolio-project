import React from 'react';
import { NavLink } from 'react-router-dom';

const pages = ["Home", "About", "Gallery", "Contact"];

// links to other pages

export default class Footer extends React.Component  {

    

    render() {
        return(
			<div className="container footer ">
				
				<div className="footer__1 bg-dark text-white">
					<div className="col-sm-12 footer-info-item">
						<div className="row">
							<div className="col-sm text-center">
								<p className="lead"><i className="material-icons" alt="Home">home</i><br/>1116 County Road 3 SW, Cokato, MN 55321</p>
							</div>
							<div className="col-sm text-center">

								<p className="lead"><i className="material-icons" alt="Telephone">phone</i><br/><a href="tel:+1-320-224-6115">Trent: (320) 224-6115</a></p>
							</div>
							<div className="col-sm text-center">
								<p className="lead"><i className="material-icons" alt="Email">email</i><br/>oakridgehardwoodmn@gmail.com</p>
							</div>

						</div>
					</div>
				</div>

				{/*Copyright, etc*/}
				<div className="small-print">
					<div className="container text-center footer-info-item">
					
						<p>Copyright &copy; 2016 - {new Date().getFullYear()} Oak Ridge Hardwood</p>
					</div>
				</div>
				
				
			</div>
    )

    }
}
