import React from 'react';
import { Link } from 'react-router-dom';

import quality from '../../public/pictures/quality-banner.JPG';
import relations from '../../public/pictures/relations-banner.JPG';


export default class HomePage extends React.Component {

  image_cap({title, message, pageDirect}, image) {

    //const picture = require(img_url);

    return (
      <div className="container card mb-3 main-box photo-cap__container">
        <div className="container photo-cap">
        
          <img className="card-img-top photo-cap__image" src={image} alt="Card image cap" />
          <div className="card-body">
          <h2 className="card-title"><b>{title}</b></h2>
            <p className="card-text">{message}</p>
            <Link className="btn btn-dark btn-lg" to={`/${pageDirect}`} >See {pageDirect}</ Link>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="container">
        <div className="container main-box">
          <h1>Welcome!</h1>
        </div>
        <div className="photo-cap__layout">
          {this.image_cap({
            title: "What We Do",
            message: "We build lasting relationships and quality hardwood floors.",
            pageDirect: "Services"
          }, quality)}
          {this.image_cap({
            title: "Who We Serve",
            message: "We serve the metro and greater minnesota with 18+ years of experience.",
            pageDirect: "About"
          }, relations)}
        </div>
      </div>
    )
  }

}