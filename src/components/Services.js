import React from 'react';

import { descriptions } from '../descriptions/descriptions.js';

export default class Services extends React.Component {
    
  displayServices(object) {
    const keys = Object.keys(object);
    
    return keys.map((key, index) => {
  
      return (
        <div key={key}>
          <div className="card">
    <div className="card-header" id={`heading${index}`}>
      <h5 className="mb-0">
        <button className="btn btn-link collapsed" data-toggle="collapse" data-target={`#collapse${index}`} aria-expanded="true" aria-controls={`collapse${index}`}>
          <h3 className="btn-link">{key}</h3>
        </button>
      </h5>
    </div>

    <div id={`collapse${index}`} className="collapse" aria-labelledby={`heading${index}`} data-parent="#accordion">
      <div className="card-body">
        <p>{object[key]}</p>
       </div>
    </div>
  </div>
          
      </div>
      )
  
    })
  }

  getServices() {
    return (
      <div id="accordian">
          {this.displayServices(descriptions)}
      </div>
    )
  }

  render() {
    const statement = "There are many different services that an interested home owner can choose from." + 
    " Below, in each section (click to expand), is contained a brief summary that describes what we have to offer."
    return (
      <div className="container">
        <div className="row">
            <div className="col-sm">
                <div >
                    <div className="card-header bg-dark">
                      <h2 className="text-white padded-header"><i>{statement}</i></h2>
                    </div>
                    
                    {this.getServices()}
                    
                </div>
                
            </div>
        </div>
    </div>
    )

  }
}