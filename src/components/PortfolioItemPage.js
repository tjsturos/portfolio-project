import React from 'react';

const PortfolioItemPage = (props) => (
    <div>
        This is portfolio item with the id of : {props.match.params.id}
    </div>
)

export default PortfolioItemPage;