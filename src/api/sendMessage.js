const axios = require('axios');
const keys = require('../keys.json');

const sendMessage = ({ to, from, body }) => {
    
        try {
            axios({
                method: 'post',
                url: 'https://dbulgq1qie.execute-api.us-east-1.amazonaws.com/prod/sms-in',
                data: JSON.stringify({
                  to,
                  from,
                  body
                }),
                headers: {
                    'x-api-key': keys['aws-lambda']
                }
              })
              return true
        } catch (e) {
            console.log("Error: ", e)
            return false
        }

   
    
   
}

// sendMessage({
//     to: "7633989461",
//     from: "7723616980",
//     body: `Testing`
//   })

export default sendMessage;