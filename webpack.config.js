const path = require('path');

module.exports = (env, args) => {

  const isProduction = env === 'production';

  return {
    entry: ['babel-polyfill', './src/app.js'],
    output: {
        path: path.join(__dirname, 'public', 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
          {
            test: /\.txt$/,
            use: 'raw-loader'
          }, 
          {
            loader: 'file-loader',
            test: /\.(jpe?g|png|gif|svg|JPE?G)$/i,
            options: {
              query: {
                name:'[name].[ext]'
              }
            }
          },
          {
            loader: 'image-webpack-loader',
            test:  /\.(gif|png|jpe?g|svg)$/i,
            options: {
              query: {
                mozjpeg: {
                  progressive: true,
                },
                gifsicle: {
                  interlaced: true,
                },
                optipng: {
                  optimizationLevel: 7,
                }
              }
            }
          }, {
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, {
            test: /\.s?css$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ] 
        }]
    },
    devtool: isProduction ? 'source-map' : 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public', 'dist'),
        historyApiFallback: true
    }
  }
};
